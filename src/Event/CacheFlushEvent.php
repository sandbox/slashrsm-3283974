<?php

declare(strict_types=1);

namespace Drupal\admin_cache_controller\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Cache flush event.
 */
final class CacheFlushEvent extends Event {

  public const ID = 'admin_cache_controller.invalidate_tags';

  /**
   * Array of cache tags to be invalidated.
   *
   * @var string[]
   */
  private array $tags;

  /**
   * CacheFlushEvent constructor.
   *
   * @param string[] $tags
   *   Array of cache tags to be invalidated.
   */
  public function __construct(array $tags) {
    $this->tags = $tags;
  }

  /**
   * Returns the cache tags.
   *
   * @return string[]
   *   Array of cache tags to be invalidated.
   */
  public function getTags(): array {
    return $this->tags;
  }

}
