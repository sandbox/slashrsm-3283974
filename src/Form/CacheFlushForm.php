<?php

declare(strict_types=1);

namespace Drupal\admin_cache_controller\Form;

use Drupal\admin_cache_controller\Event\CacheFlushEvent;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Cache flush form.
 */
final class CacheFlushForm extends FormBase {

  /**
   * The event dispatcher.
   *
   * @var EventDispatcherInterface
   */
  private EventDispatcherInterface $dispatcher;

  /**
   * A logger channel.
   *
   * @var LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * Cache tags invalidator.
   *
   * @var CacheTagsInvalidatorInterface
   */
  private CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * CacheFlushForm constructor.
   */
  public function __construct(
    EventDispatcherInterface $dispatcher,
    LoggerChannelFactoryInterface $logger_factory,
    CacheTagsInvalidatorInterface $cache_invalidator
  ) {
    $this->dispatcher = $dispatcher;
    $this->logger = $logger_factory->get('admin_cache_controller');
    $this->cacheTagsInvalidator = $cache_invalidator;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('logger.factory'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_cache_controller_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'inline_template',
      '#template' => '<div>{{ paragraph1 }}</div></br><div>{{ paragraph2 }}</div>',
      '#context' => [
        'paragraph1' => $this->t('Cache clear is an intrusive operation, that has significant performance implications. It should be used carefully and only when strictly required due to the content changes not appearing on the page.'),
        'paragraph2' => $this->t("Please describe the reason for the cache clear. Most importantly, which content item was updated and where (which page, which part of the page) the content wasn't updated on the page.")],

    ];
    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment'),
      '#required' => TRUE,
      '#rows' => 3,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear render cache')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tags_to_invalidate = ['rendered'];
    $current_user = $this->currentUser();
    $this->logger->info('User {name} (UID {id}) cleared render cache with comment: "{comment}"', [
      'name' => $current_user->getDisplayName(),
      'id' => $current_user->id(),
      'comment' => $form_state->getValue('comment'),
    ]);
    $event = new CacheFlushEvent($tags_to_invalidate);
    $this->dispatcher->dispatch($event, CacheFlushEvent::ID);
    $this->cacheTagsInvalidator->invalidateTags($event->getTags());
    $this->messenger()->addMessage($this->t('Render cache was cleared successfully.'));
  }
}
